#include <iostream>
#include <vector>
#include <map>
#include <mpir.h>

using namespace std;

vector<unsigned int> get_primes(unsigned int upper, mpz_t N) {
	mpz_t p;
	mpz_init_set_ui(p, 2);

	vector<unsigned int> primes;
	primes.push_back(2);

	while (mpz_cmp_ui(p, upper) <= 0) {
		mpz_nextprime(p, p);
		if (mpz_jacobi(N, p) == 1) {
			primes.push_back(mpz_get_ui(p));
		}
	}

	return primes;
}

void mod_root_z(mpz_t R, mpz_t n, mpz_t p) {
	mpz_t Q, S;
	mpz_init_set(Q, p);
	mpz_sub_ui(Q, Q, 1);
	mpz_init_set_ui(S, 0);

	mpz_t tmp;
	mpz_init(tmp);

	mpz_mod_ui(tmp, Q, 2);

	while (mpz_cmp_ui(tmp, 0) == 0) {
		mpz_add_ui(S, S, 1);
		mpz_div_ui(Q, Q, 2);
		mpz_mod_ui(tmp, Q, 2);
	}

	mpz_t z, t, c, M;
	mpz_init_set_ui(z, 0);
	mpz_inits(t, c, M, NULL);

	while (mpz_cmp(z, p) < 0) {
		if (mpz_jacobi(z, p) == -1) {
			break;
		}

		mpz_add_ui(z, z, 1);
	}

	mpz_set(M, S);
	mpz_powm(c, z, Q, p);

	mpz_powm(t, n, Q, p);
	mpz_add_ui(tmp, Q, 1);
	mpz_div_ui(tmp, tmp, 2);

	mpz_powm(R, n, tmp, p);

	while (mpz_cmp_ui(t, 0) != 0 && mpz_cmp_ui(t, 1)) {
		unsigned int i = 1;

		while (mpz_cmp_ui(M, i) > 0) {
			mpz_powm_ui(tmp, t, (unsigned int)pow(2, i), p);
			if (mpz_cmp_ui(tmp, 1) == 0) {
				break;
			}

			++i;
		}

		mpz_powm_ui(tmp, c, (unsigned int)pow(2, mpz_get_ui(M) - i - 1), p);

		mpz_set_ui(M, i);
		mpz_powm_ui(c, tmp, 2, p);
		mpz_mul(t, t, tmp);
		mpz_mul(t, t, tmp);
		mpz_mod(t, t, p);
		mpz_mul(R, R, tmp);
		mpz_mod(R, R, p);
	}

	mpz_clears(Q, S, tmp, z, t, c, M, NULL);

	if (mpz_cmp_ui(t, 0) == 0) {
		mpz_set_ui(R, 0);
	}
}


void next_polynomial(mpz_t A, mpz_t B, mpz_t D, unsigned int M, mpz_t N) {
	mpz_nextprime(D, D);

	while (mpz_jacobi(N, D) != 1) {
		mpz_nextprime(D, D);
	}

	mpz_t h1, h2, tmp;
	mpz_inits(h1, h2, tmp, NULL);
	mod_root_z(h1, N, D);

	mpz_mul_ui(h2, h1, 2);
	mpz_invert(h2, h2, D);

	mpz_pow_ui(tmp, h1, 2);
	mpz_sub(tmp, N, tmp);
	mpz_div(tmp, tmp, D);
	mpz_mul(h2, h2, tmp);
	mpz_mod(h2, h2, D);

	mpz_mul(A, D, D);

	mpz_mul(h2, h2, D);
	mpz_add(h2, h2, h1);
	mpz_mod(B, h2, A);

	mpz_mod_ui(tmp, B, 2);
	if (mpz_cmp_ui(tmp, 0) == 0) {
		mpz_sub(B, A, B);
	}

	mpz_clears(h1, h2, tmp, NULL);
}

vector<unsigned int> trial_divide(vector<unsigned int> prime_base, mpz_t remainder) {
	mpz_t tmp;
	mpz_init(tmp);

	vector<unsigned int> matrix_row(prime_base.size() + 1);  // Extra column for the -1 factor

	if (mpz_cmp_ui(remainder, 0) < 0) {
		matrix_row[0] = 1;
		mpz_neg(remainder, remainder);
	}

	for (int j = 0; j < prime_base.size(); ++j) {
		int flipper = 0;
		while (mpz_mod_ui(tmp, remainder, prime_base[j]) == 0) {
			mpz_div_ui(remainder, remainder, prime_base[j]);
			flipper ^= 1;
		}
		matrix_row[j + 1] = flipper;  // j+1 because first 'prime' is -1

		if (mpz_cmp_ui(remainder, 1) == 0) {
			break;
		}

		if (mpz_cmp_ui(remainder, pow(prime_base[prime_base.size() - 1], 2)) == 0) {
			break;
		}
	}

	mpz_clear(tmp);

	return matrix_row;
}

struct partial_relation {
	string x;
	string y;
};

int main(void) {
	// Input n
	mpz_t N;
	mpz_init_set_str(N, "1485861227581829634826821615821280605846054443085657950889939722778903", 10);

	// Calculate k such that kn = 1 mod 4
	mpz_t kn_mod, tmp, tmp2, res;
	mpz_inits(kn_mod, tmp, res, tmp2, NULL);

	mpz_t remainder;
	mpz_init(remainder);

	//int k = 1;
	//mpz_mod_ui(kn_mod, N, 4);

	//while (mpz_cmp_ui(kn_mod, 1) != 0) {
	//	++k;
	//	mpz_mul_ui(tmp, N, k);
	//	mpz_mod_ui(kn_mod, tmp, 4);
	//}

	//mpz_mul_ui(N, N, k);

	mpz_out_str(stdout, 10, N);
	cout << endl;

	// Input M
	int M = 200000;

	// Input F
	unsigned int F = 110000;

	// Calculate factor base of primes from 2 - F having jacobi symbol 1 mod kn
	vector <unsigned int> primes = get_primes(F, N);
	vector <unsigned int> roots{ 1 };
	vector <unsigned int> sizes{ 2 };

	vector<vector<unsigned int>> matrix;
	vector<vector<unsigned int>> identity;  // Keeps a list of indices of all X's which have been combined

	cout << "Solving roots" << endl;
	// Solve modular roots for each prime p and store
	for (int i = 1; i < primes.size(); ++i) {
		mpz_set_ui(tmp, primes[i]);
		mod_root_z(res, N, tmp);
		roots.push_back(mpz_get_ui(res));
		sizes.push_back(log10(primes[i]));
	}

	cout << "Finished" << endl;

	cout << "Generating polynomial" << endl;
	// Generate a polynomial
	mpz_t A, B, D;
	mpz_t ainv, rootp, pz;
	mpz_inits(A, B, D, NULL);
	mpz_inits(ainv, rootp, pz, NULL);

	
	
	mpz_div_ui(D, N, 2);
	mpz_sqrt(D, D);
	mpz_div_ui(D, D, M);
	mpz_sqrt(D, D);

	unsigned int smooths = 0;
	map<string, partial_relation> partials;
	unsigned int bigprimes = 0;

	mpz_t* X;
	mpz_t* Y;

	X = (mpz_t*)malloc(sizeof(mpz_t) * (primes.size() + 2));  // +1 for -1 factor, +1 because we need 1 more relation than primes
	Y = (mpz_t*)malloc(sizeof(mpz_t) * (primes.size() + 2));

	for (int i = 0; i < primes.size() + 2; ++i) {
		mpz_init(X[i]);
		mpz_init(Y[i]);
	}

	mpz_t dbg;
	mpz_init(dbg);

	mpz_t dinv;
	mpz_init(dinv);

	while (smooths + bigprimes < primes.size() + 2) {
		next_polynomial(A, B, D, M, N);

		mpz_invert(dinv, D, N);

		// Solve modular inverses
		vector<unsigned int> soln1(primes.size());
		vector<unsigned int> soln2(primes.size());

		for (int i = 0; i < primes.size(); ++i) {
			mpz_set_ui(pz, primes[i]);
			mpz_invert(ainv, A, pz);
			mpz_set_ui(tmp, roots[i]);
			mpz_sub(tmp, tmp, B);
			mpz_mul(tmp, tmp, ainv);
			mpz_mod(tmp, tmp, pz);
			soln1[i] = mpz_get_ui(tmp);

			mpz_set_si(tmp, (int)-1 * roots[i]);
			mpz_sub(tmp, tmp, B);
			mpz_mul(tmp, tmp, ainv);
			mpz_mod(tmp, tmp, pz);
			soln2[i] = mpz_get_ui(tmp);
		}

		// Sieve
		vector<unsigned int> sieve((2 * M) + 1);

		for (int i = 15; i < primes.size(); ++i) {
			for (int j = 0; soln1[i] + (j * primes[i]) <= M; ++j) {
				unsigned int pos_idx = M + (soln1[i] + (j * primes[i]));
				unsigned int neg_idx = M + (soln1[i] + (-j * primes[i]));
				sieve[pos_idx] += sizes[i];

				if (neg_idx != pos_idx) {
					sieve[neg_idx] += sizes[i];
				}
			}

			if (primes[i] == 2) {
				continue;
			}

			for (int j = 0; soln2[i] + (j * primes[i]) <= M; ++j) {
				unsigned int pos_idx = M + (soln2[i] + (j * primes[i]));
				unsigned int neg_idx = M + (soln2[i] + (-j * primes[i]));
				sieve[pos_idx] += sizes[i];

				if (pos_idx != neg_idx) {
					sieve[neg_idx] += sizes[i];
				}
			}
		}

		// Trial divide
		mpz_sqrt(tmp, N);
		mpz_mul_ui(tmp, tmp, M);
		unsigned int T = mpz_sizeinbase(tmp, 10);
		float thresh = T;
		thresh *= 0.6;

		//if (smooths == 61 && i == 38707) {
			//cout << "bh" << endl;
		//}


		for (int i = 0; i < sieve.size(); ++i) {
			if (smooths + bigprimes >= primes.size() + 2) {
				break;
			}
			if (sieve[i] >= thresh) {
				// Calculate (Ax + B)
				mpz_mul_si(tmp, A, i - M);
				mpz_add(tmp, tmp, B);

				mpz_mul(tmp2, tmp, dinv);
				mpz_mod(tmp2, tmp2, N);
				mpz_set(X[smooths + bigprimes], tmp2);

				// Calculate X^2 - N / A
				mpz_mul(tmp, tmp, tmp);
				mpz_sub(tmp, tmp, N);
				mpz_div(remainder, tmp, A);
				mpz_set(Y[smooths + bigprimes], remainder);

				// Call trial division function here!
				vector<unsigned int> matrix_row = trial_divide(primes, remainder);

				if (mpz_cmp_ui(remainder, primes[primes.size() - 1]) < 0) {
					matrix.push_back(matrix_row);

					vector<unsigned int> identity_row(primes.size() + 2);
					identity_row[smooths + bigprimes] = 1;
					identity.push_back(identity_row);
					++smooths;
				}
				else if (mpz_cmp_ui(remainder, pow(primes[primes.size() - 1], 2)) < 0) {
					string ul_remainder = mpz_get_str(NULL, 10, remainder);
					if (partials.find(ul_remainder) == partials.end()) {
						partial_relation pr = { mpz_get_str(NULL, 10, X[smooths + bigprimes]), mpz_get_str(NULL, 10, Y[smooths + bigprimes]) };
						partials[ul_remainder] = pr;
					}
					else {
						// Handle partial here
						// This took hours to figure out, the answer was on the programming praxis website
						// The key is to multiply X and Y by the inverse of D mod N, where D is the square root of A
						// They are pre-multiplied by dinv a few dozen lines above here

						// tmp = previous X value
						// combined = tmp * current X value * (shared prime inverted) mod N
						mpz_set_str(tmp, partials[ul_remainder].x.c_str(), 10);
						mpz_mul(tmp, tmp, X[smooths + bigprimes]);
						mpz_set_str(tmp2, ul_remainder.c_str(), 10);
						mpz_invert(tmp2, tmp2, N);
						mpz_mul(tmp, tmp, tmp2);
						mpz_mod(tmp, tmp, N);


						// Save the X and Y relations
						mpz_set(X[smooths + bigprimes], tmp);

						// Update relation
						// partials[ul_remainder] = mpz_get_str(NULL, 10, X[smooths + bigprimes]);
						//partials.erase(ul_remainder);

						mpz_set_str(tmp, partials[ul_remainder].y.c_str(), 10);
						mpz_set_str(tmp2, ul_remainder.c_str(), 10);
						mpz_mul(tmp, tmp, Y[smooths + bigprimes]);
						mpz_div(tmp, tmp, tmp2);
						mpz_div(tmp, tmp, tmp2);
						
						mpz_set(Y[smooths + bigprimes], tmp);
						cout << "DEBUG: Combined Y = ";
						mpz_out_str(stdout, 10, Y[smooths + bigprimes]);
						cout << endl;


						vector<unsigned int> matrix_row = trial_divide(primes, tmp);
						matrix.push_back(matrix_row);

						vector<unsigned int> identity_row(primes.size() + 2);
						identity_row[smooths + bigprimes] = 1;
						identity.push_back(identity_row);

						// partials.erase(ul_remainder);

						++bigprimes;
					}
				}
			}
		}

		cout << "Found " << smooths + bigprimes << " smooths (" << bigprimes << " from partials) (goal: " << primes.size() + 1 << ")" << endl;

		if (smooths + bigprimes == 624) {
			cout << "bh" << endl;
		}
	}

	// This is only for debugging, output all X and Y relations in CSV, I suspect some partial relations may be creating duplicates
	for (int i = 0; i < primes.size() + 2; ++i) {
		mpz_out_str(stdout, 10, X[i]);
		cout << ",";
		mpz_out_str(stdout, 10, Y[i]);
		cout << endl;
	}
	

	// Linear algebra
	unsigned int counter = 0;
	unsigned int pivot = 0;
	
	for (int col = primes.size(); col >= 0; --col) {
		cout << "col " << col << endl;
		// Find the first row with a 1 in the current col
		bool found = false;
		for (int row = counter; row < matrix.size(); ++row) {
			if (matrix[row][col] == 1) {
				pivot = row;
				found = true;
				break;
			}
		}

		if (found) {
			// Now that the first row has been found, swap it with row /counter/
			vector<unsigned int> buffer = matrix[counter];
			matrix[counter] = matrix[pivot];
			matrix[pivot] = buffer;

			// Also swap the identity rows
			buffer = identity[counter];
			identity[counter] = identity[pivot];
			identity[pivot] = buffer;

			// Now find each subsequent row with a 1 in the current column
			for (int row = counter + 1; row < matrix.size(); ++row) {
				if (matrix[row][col] == 1) {
					// Perform element-wise sum with matrix[counter] and matrix[row], storing the result in matrix[row], all over GF(2)
					for (int i = 0; i < matrix[row].size(); ++i) {
						matrix[row][i] ^= matrix[counter][i];
					}

					// Add all elements from identity[counter] to identity[row]
					for (int i = 0; i < identity[row].size(); ++i) {
						identity[row][i] ^= identity[counter][i];
					}
					//identity[row].reserve(identity[row].size() + distance(identity[counter].begin(), identity[counter].end()));
					//identity[row].insert(identity[row].end(), identity[counter].begin(), identity[counter].end());
					
				}
			}

			// Finally, increment the counter and start all over again
			++counter;
		}
	}

	mpz_t Yfinal, Xfinal;
	mpz_inits(Yfinal, Xfinal, NULL);

	// Now we can actually start looking for solutions!
	for (int row = 0; row < matrix.size(); ++row) {
		bool all_even = true;
		for (int col = 0; col < matrix[row].size(); ++col) {
			if (matrix[row][col] == 1) {
				all_even = false;
				break;
			}
		}

		if (all_even) {
			// Start by calculating Y
			mpz_set_ui(Yfinal, 1);
			mpz_set_ui(Xfinal, 1);
			for (int i = 0; i < identity[row].size(); ++i) {
				if (identity[row][i] == 1) {
					mpz_mul(Yfinal, Yfinal, Y[i]);
					mpz_mul(Xfinal, Xfinal, X[i]);
				}
			}

			// If everything has been done correctly up to this point, Yfinal should be a perfect square
			mpz_sqrt(Yfinal, Yfinal);

			// Fingers crossed!
			mpz_sub(tmp, Xfinal, Yfinal);
			mpz_gcd(tmp, tmp, N);

			if (mpz_cmp_ui(tmp, 1) != 0 && mpz_cmp(tmp, N) != 0) {
				cout << "FOUND FACTOR!!!!!!!!!: ";
				mpz_out_str(stdout, 10, tmp);
				cout << endl;
				return 0;
			}
		}
	}




	return 0;
}